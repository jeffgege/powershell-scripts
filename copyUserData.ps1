#This is a script that is used to move files
#for users changing computers
#Author: Jeff Gegetskas
#Date Implemented: 
#Version: 1.0

#Script paramaters DO NOT EDIT 
Param(
	[switch]$ImportData,
	[switch]$ExportData,
	[switch]$Help,
	[string]$Username,
	[string]$ZipFilePath
)

#These are the folders and files that are going to be copied
$folders = @('Desktop', 'Documents', 'Downloads', 'Favorites', 'Pictures', 'AppData\Roaming\Microsoft\Signatures');
$files = @('AppData\Local\Microsoft\Outlook\RoamCache\Stream_Autocomplete_*.*');

#Error funtion this is called on user error
function Error {
	Write-Host $args[0] -foregroundcolor "Red";
	Exit
}

#Check if the users running the script is an admin
#If not error and exit the script
$isAdmin = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if(!$isAdmin.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
	Error "You must run this script as an administrator";
	Exit
}

#If someone uses the -Help switch it shows this message
if($Help) {
	Write-Host "Paramaters `n`n -Help: `t Displays this message `n -ImportData: `t Grabs data from the current computer -ZipFilePath will be required `n -ExportData: `t Will export data from zip file. -ZipFilePath will be required `n -Username: `t This specifies the username where data is kept `n -ZipFilePath: `t Indicates the path of the zip file this is used for both import and export`n";
	Exit
}

#Check if the username switch is used
#This is a required switch
if(!$username) {
	Error "Username is required"
	Exit
}
#This adds the ability to create and extract zip files
Add-Type -assembly "system.io.compression.filesystem"

#This is the export data function
if($ExportData) {
	#Check if the ZipFilePath is set
	if(!$ZipFilePath) {
		Error "You must use the -ZipFile switch to set path for zipfile output"
		Exit
	}
	
	#Variables used for export data
	$sourceDirectory = "C:\Users\" + $username + "\";
	$directoryName = $username + "_export";
	$DestinationDirectory = $ZipFilePath + $directoryName + "\";
	$exclude = @('My Music', 'My Pictures', 'My Videos', 'desktop.ini');
	
	#Check and see if export folder is created
	#If it is remove it
	if(Test-path $DestinationDirectory) {
		Remove-item $DestinationDirectory -recurse
		Write-Host "Removing old directory";
	}
	
	#Go through all requested backup folders and 
	#move them to the archive folder
	foreach ($folder in $folders) {
		$copyDirectory = $sourceDirectory + $folder + "\"
		$Destination = $DestinationDirectory + $folder;
		
		#Make sure path exists and move it
		#Otherwise show message it doesn't exist
		if(Test-Path $copyDirectory) {
			Copy-Item $copyDirectory -Destination ($Destination + '\') -recurse -exclude $exclude
			Write-Host "Copying" $folder "`n";
		} else {
			Write-Host "Ommiting" $folder "it does not exist `n";
		}
	}
	
	#Go through all requested backup files and
	#move them to the root of the archive folder
	foreach ($file in $files) {
		$copyFile = $sourceDirectory + $file;
		
		#Check to make sure the path exists
		#If it does move it otherwise show message
		if(Test-Path $copyFile) {
			$names = Get-ChildItem -path $copyFile | sort-object {$_.LastWriteTime} -Descending | select-object -First 1
			foreach($name in $names) {
				$fromPath = $sourceDirectory + "\" + $name.Name;
				$toPath = $DestinationDirectory + "\" + $name.Name;
				if(Test-Path $fromPath) {
					Copy-Item $fromPath -Destination $toPath -Force
					Write-Host "Copying" $file "`n";
				} else {
					Write-Host "Ommiting" $file "it does not exist `n";
				}
			}
		} else {
			Write-Host "Omitting" $file "it does not exist `n";
		}
	}
	
	#Check if the zip folder is there for
	#Some unknown reason or if you already ran it
	$ZipDestination = $ZipFilePath + $directoryName + ".zip";
	if(Test-path $ZipDestination) {
		Remove-item $ZipDestination -recurse
		Write-Host "Removing old zip directory";
	}
	
	#Once all files and folders are moved zip up the folder
	#The original folder will stay there so it can be checked
	#TODO:Remove to be archive folder 
	Add-Type -assembly "system.io.compression.filesystem"
	[io.compression.zipfile]::CreateFromDirectory($DestinationDirectory, $ZipDestination)
	Write-Host "Directory has been zipped here" $ZipDestination -foregroundcolor "Green";
}
#This is the import data function
if($ImportData) {
	#Make sure zip path is set
	if(!$ZipFilePath) {
		Error "You must use the -ZipFile switch to set path for zipfile output"
		Exit
	}
	#Define extract directory and make sure it doesn't exist.
	#If it does remove the directory
	$ExtractDirectory = "C:\Tmp\UserDataExtraction\";
	if(Test-Path $ExtractDirectory) {
		#Do nothing path exists. 
		#TODO: Fix to not have to do it this way
	} else {
		New-Item -ItemType directory -Path $ExtractDirectory
	}
	
	$directoryName = $username + "_export";
	$ZipDestination = $ZipFilePath + $directoryName + ".zip";
	[io.compression.zipfile]::ExtractToDirectory($ZipDestination, $ExtractDirectory)
	$DestinationDirectory = "C:\Users\" + $username + "\";
	foreach ($folder in $folders) {
		$copyDirectory = $ExtractDirectory + $folder;
		$DestinationDirectory = "C:\Users\" + $username + "\";
		if(Test-Path $copyDirectory) {
			Copy-Item $copyDirectory -Destination $DestinationDirectory -recurse -exclude $exclude
			Write-Host "Copying" $folder "`n";
		} else {
			Write-Host "Ommiting" $copyDirectory "it does not exist `n";
		}
	}
	foreach ($file in $files) {
		$copyFile = $ExtractDirectory + $file;
		if(Test-Path $copyFile) {
			$names = Get-ChildItem -path $copyFile | sort-object {$_.LastWriteTime} -Descending | select-object -First 1
			foreach($name in $names) {
				$fromPath = $ExtractDirectory + "\" + $name.Name;
				$toPath = $DestinationDirectory + "\" + $name.Name;
				if(Test-Path $fromPath) {
					Copy-Item $fromPath -Destination $toPath -Force
					Write-Host "Copying" $file "`n";
				} else {
					Write-Host "Ommiting" $file "it does not exist `n";
				}
			}
		} else {
			Write-Host "Omitting" $file "it does not exist `n";
		}
	}
}