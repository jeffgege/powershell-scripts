#This is a script that is used to move files
#for users changing computers
#Author: Jeff Gegetskas
#Date Implemented: 
#Version: 1.0

# To Run the script run .\disableUser.ps1 -username myuser -ou mycompany -dc com or .\disableUser.ps1 myuser mycompany com
# Replace myuser with the username you wish to disable replacec company with your OU and com with your DC
Param(
	[string]$username
	[string]$dc
	[string]$ou
)

function Error {
	Write-Host $args[0] -foregroundcolor "Red";
	Exit
}

function Inform {
	Write-Host $args[0] -foregroundcolor "Green";
}

function createPassword {
	$chars = "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*/?".ToCharArray()
	$password = "";
	for ($x = 0; $x -lt 12; $x++) {
		$password += $set | Get-Random
	}
	return ConverTo-SecureString -String $password -AsPlainText -Force
}

#Set new password for username
$newPassword = createPassword
Set-ADAccountPassword $username -NewPassword $newPassword -Reset -PassThru

#Get groups user is a part of and write it to a text file 
#This will get added to the user drive
GetADPrincipalGroupMembership $username | select name | Out-File groups.txt

#Remove the user from all groups
Get-ADUser -Identity $username -Properties MemberOf | ForEach-Object {
	$_.MemberOf | Remove-ADGroupMember -Members $_.DistinguishedName -Confirm:$false
}

#Disable AD account
Disable-ADAccount -Identity $username

#Move user to disabled OU
Get-ADUser $username | Move-ADObject -TargetPath 'OU=Disabled_Users,DC=$dc,DC=$ou'

